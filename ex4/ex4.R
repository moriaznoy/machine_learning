install.packages('ggplot2')
library(ggplot2)

install.packages("ggplot2movies")
library(ggplot2movies)

EXmovies<-movies
str(EXmovies)

#Q1
ggplot(EXmovies, aes(rating))+geom_histogram()
#���� ����� ���� �������� ������ ����� ����, ����� ��� ������� �������

#Q2
ggplot(EXmovies, aes(year,rating))+stat_smooth(method=lm)

#Q3
ggplot(EXmovies, aes(year,budget))+stat_smooth(method=lm)

#Q4
breaks <- c(1,60,120,180,240,9999)
labels <- c("very short length","short length", "regular length", "long length", "very long length")
bins <- cut(EXmovies$length, breaks, include.lowest = T,right = F, labels=labels)
EXmovies$length<-bins
ggplot(EXmovies, aes(length,budget))+ geom_boxplot()

#Q5
ggplot(EXmovies, aes(length,rating))+ geom_boxplot()

#Q6
breaks <- c(1,3,5,7,9,11)
labels <- c("very low","low", "medium", "medium plus", "good","very good")
bins <- cut(EXmovies$length, breaks, include.lowest = T,right = F, labels=labels)
EXmovies$rating<-bins
ggplot(EXmovies, aes(rating, budget))+geom_boxplot()

#Q7
EXmovies<-movies
EXmovies$Animation <- factor(EXmovies$Animation,levels=c(0,1),  labels = c( "No Animation", "Animation"))
ggplot(EXmovies,aes(Animation,rating))+geom_boxplot()

#Q8
EXmovies$Action <- factor(EXmovies$Action,levels=c(0,1),  labels = c( "No Action", "Action"))
ggplot(EXmovies, aes(Action, rating)) + geom_boxplot()
